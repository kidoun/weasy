package org.weasy.test.api.rest.service.apiRest.annonceImmobiliere;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.weasy.test.api.rest.service.annonceImmobiliere.IAnnonceImmobiliereService;
import org.weasy.test.api.rest.service.dto.AnnonceImmobiliereDto;

@RestController
@RequestMapping("/app/rest")
public class AnnonceImmobiereRessource {

	private static final String SUCCESS = "success";

	private static final Logger logger = Logger.getLogger(AnnonceImmobiereRessource.class);

	@Autowired
	private IAnnonceImmobiliereService annonceImmobiliereService;

	// list of annonceImmobiliers
	@RequestMapping(value = "/annonceImmobilieres", method = RequestMethod.GET)
	public ResponseEntity<List<AnnonceImmobiliereDto>> getListAnnonceImmobilieres()  {

		List<AnnonceImmobiliereDto> listAnnonceImmobilieres;
		try {
			listAnnonceImmobilieres = annonceImmobiliereService.GetListOfAllAnnonceImmobiliere();
			return new ResponseEntity<>(listAnnonceImmobilieres, HttpStatus.OK);
		} catch (Exception e) {
			logger.error("error while reteiving List of annonceImmobilieres", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

		
	}

	// reteive annonceImmobiliere by id
	@RequestMapping(value = "/annonceImmobiliere", method = RequestMethod.GET)
	public ResponseEntity<AnnonceImmobiliereDto> getAnnonceImmobiliere(
			@RequestParam("id") final Long id)  {

		AnnonceImmobiliereDto listAnnonceImmobilieres;
		try {
			listAnnonceImmobilieres = annonceImmobiliereService.FindAnnonceImmobiliereById(id);
			return new ResponseEntity<>(listAnnonceImmobilieres, HttpStatus.OK);
		} catch (Exception e) {
			logger.error("error while reteiving annonceImmobiliere", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	// retrieve annonceImmobiliere by id
	@RequestMapping(value = "/addAnnonceImmobiliere", method = RequestMethod.POST)
	public ResponseEntity<String> getAnnonceImmobiliere(
			@RequestParam("titre") String titre,
            @RequestParam("descriptif") String descriptif,
            @RequestParam("adresse") String adresse,
			@RequestParam("surface") final Long surface,
			@RequestParam("prix") final Long prix) throws Exception {
		AnnonceImmobiliereDto annonceImmobiliereDto = new AnnonceImmobiliereDto();
		annonceImmobiliereDto.setTitre(titre);
		annonceImmobiliereDto.setDescriptif(descriptif);
		annonceImmobiliereDto.setAdresse(adresse);
		annonceImmobiliereDto.setSurface(surface);
		annonceImmobiliereDto.setPrix(prix);
		
		try {
			annonceImmobiliereService.InsertAnnonceImmobiliere(annonceImmobiliereDto);
			return new ResponseEntity<>(SUCCESS, HttpStatus.CREATED);
			
		} catch (Exception e) {
			logger.error("error during adding annonceImmobiliere", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

}
