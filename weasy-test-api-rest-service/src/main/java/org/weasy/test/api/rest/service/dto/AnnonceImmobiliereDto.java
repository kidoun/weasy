package org.weasy.test.api.rest.service.dto;

public class AnnonceImmobiliereDto {
	
	private Integer idAnnonceImmobiliere;
	private String titre;
	private String descriptif;
	private String adresse;
	private Long surface;
	private Long prix;
	
	public Integer getIdAnnonceImmobiliere() {
		return idAnnonceImmobiliere;
	}
	public void setIdAnnonceImmobiliere(Integer idAnnonceImmobiliere) {
		this.idAnnonceImmobiliere = idAnnonceImmobiliere;
	}
	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}
	public String getDescriptif() {
		return descriptif;
	}
	public void setDescriptif(String descriptif) {
		this.descriptif = descriptif;
	}
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public Long getSurface() {
		return surface;
	}
	public void setSurface(Long surface) {
		this.surface = surface;
	}
	public Long getPrix() {
		return prix;
	}
	public void setPrix(Long prix) {
		this.prix = prix;
	}
	@Override
	public String toString() {
		return "AnnonceImmobiliere [idAnnonceImmobiliere=" + idAnnonceImmobiliere + ", titre=" + titre + ", descriptif="
				+ descriptif + ", adresse=" + adresse + ", surface=" + surface + ", prix=" + prix + "]";
	}
	

}
