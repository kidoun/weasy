package org.weasy.test.api.rest.service.annonceImmobiliere;

import java.util.ArrayList;
import java.util.List;

import org.dozer.Mapper;
import org.dozer.spring.DozerBeanMapperFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.weasy.test.api.rest.dao.annonceImmobilere.IannonceImmobiliereDao;
import org.weasy.test.api.rest.dao.entity.AnnonceImmobiliere;
import org.weasy.test.api.rest.service.dto.AnnonceImmobiliereDto;

@Service
public class AnnonceImmobiliereServiceImpl implements IAnnonceImmobiliereService {
	
	@Autowired
	IannonceImmobiliereDao annonceImmobiliereDao;
	@Autowired
	private DozerBeanMapperFactoryBean dozerBean;

	@Override
	public List<AnnonceImmobiliereDto> GetListOfAllAnnonceImmobiliere() throws Exception {
		List<AnnonceImmobiliere> listAnnonceImmobiliere;
		List<AnnonceImmobiliereDto> listAnnonceImmobiliereDto = new ArrayList<AnnonceImmobiliereDto>();
		
		Mapper mapper = (Mapper) dozerBean.getObject();

		
		listAnnonceImmobiliere = annonceImmobiliereDao.GetListOfAllAnnonceImmobiliere();
		
		for (AnnonceImmobiliere annonceImmobiliere : listAnnonceImmobiliere) {
			
			listAnnonceImmobiliereDto.add(mapper.map(annonceImmobiliere, AnnonceImmobiliereDto.class));
			
		}
		

		return listAnnonceImmobiliereDto;
		
	}

	@Override
	public void InsertAnnonceImmobiliere(AnnonceImmobiliereDto annonceImmobiliereDto) throws Exception {
		Mapper mapper = null;
		mapper = (Mapper) dozerBean.getObject();
		AnnonceImmobiliere annonceImmobiliere = mapper.map(annonceImmobiliereDto, AnnonceImmobiliere.class);
		annonceImmobiliereDao.InsertAnnonceImmobiliere(annonceImmobiliere);;
		
	}

	@Override
	public AnnonceImmobiliereDto FindAnnonceImmobiliereById(Long id) throws Exception {
		Mapper mapper = null;
		mapper = (Mapper) dozerBean.getObject();
		AnnonceImmobiliere annonceImmobiliere = annonceImmobiliereDao.FindAnnonceImmobiliereById(id);
		AnnonceImmobiliereDto annonceImmobiliereDto = mapper.map(annonceImmobiliere, AnnonceImmobiliereDto.class);
		return annonceImmobiliereDto;
	}

}
