package org.weasy.test.api.rest.service.annonceImmobiliere;

import java.util.List;


import org.weasy.test.api.rest.service.dto.AnnonceImmobiliereDto;

public interface IAnnonceImmobiliereService {
	
	List<AnnonceImmobiliereDto> GetListOfAllAnnonceImmobiliere() throws Exception;
	AnnonceImmobiliereDto FindAnnonceImmobiliereById(Long id) throws Exception;
	void InsertAnnonceImmobiliere(AnnonceImmobiliereDto annonceImmobiliereDto) throws Exception;

}
