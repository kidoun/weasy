package org.weasy.test.api.rest.jobs.configuration.contextService;


import java.io.IOException;

import org.dozer.spring.DozerBeanMapperFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

@Configuration
@ComponentScan(basePackages = { "org.weasy.test.api.rest" })
@Import({org.weasy.test.api.rest.jobs.configuration.contextDao.ApplicationDaoContext.class})
public class ApplicationServicesContext {
	
	@Bean
	public DozerBeanMapperFactoryBean configurationDozer() throws IOException {
		
		
		DozerBeanMapperFactoryBean mapper = new DozerBeanMapperFactoryBean();
		Resource[] resources = new PathMatchingResourcePatternResolver()
				.getResources("classpath*:dozer-bean-mappings.xml");
		mapper.setMappingFiles(resources);
		

		return mapper;
	}


}
