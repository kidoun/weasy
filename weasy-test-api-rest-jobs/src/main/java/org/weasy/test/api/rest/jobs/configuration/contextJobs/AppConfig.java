package org.weasy.test.api.rest.jobs.configuration.contextJobs;




import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;




@SpringBootApplication(exclude = {SecurityAutoConfiguration.class })
@Configuration
@ComponentScan(basePackages = { "org.weasy.test.api.rest" })
@Import({org.weasy.test.api.rest.jobs.configuration.contextService.ApplicationServicesContext.class})
public class AppConfig {

	
	/**
	 * Initialisation de configuration.
	 * 
	 * @return ClassPathResource
	 */
	@Bean
	static ClassPathResource classPathResource() {
		return new ClassPathResource("config.properties");
	}

	
	/*
	 * Placeholders
	 */
	/**
	 * Configuration Placeholder interne.
	 * 
	 * @return PropertySourcesPlaceholderConfigurer
	 */
	static @Bean public PropertySourcesPlaceholderConfigurer internalPlaceHolderConfigurer() {
		PropertySourcesPlaceholderConfigurer placeholder = new PropertySourcesPlaceholderConfigurer();
		placeholder.setLocation(classPathResource());
		placeholder.setOrder(2);
		placeholder.setIgnoreUnresolvablePlaceholders(false);
		placeholder.setIgnoreResourceNotFound(false);
		return placeholder;
	}
   
	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurerAdapter() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**");
			}
		};
	}
	


}
