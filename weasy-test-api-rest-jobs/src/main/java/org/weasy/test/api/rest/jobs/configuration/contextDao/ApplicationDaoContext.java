package org.weasy.test.api.rest.jobs.configuration.contextDao;

import org.weasy.test.api.rest.jobs.configuration.contextDao.DataSourceContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan(basePackages = { "org.weasy.test.api.rest.dao" })
@Import({ DataSourceContext.class })
public class ApplicationDaoContext {

}
