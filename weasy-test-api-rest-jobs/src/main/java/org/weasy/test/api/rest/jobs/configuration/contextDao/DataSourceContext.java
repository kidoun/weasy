package org.weasy.test.api.rest.jobs.configuration.contextDao;





import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
public class DataSourceContext {
	private static final Logger logger = Logger.getLogger(DataSourceContext.class);
	@Value("${datasource.weasy.driverclass}")
	private String driverclassWeasy;

	@Value("${datasource.weasy.url}")
	private String urlWeasy;

	@Value("${datasource.weasy.username}")
	private String usernameWeasy;

	@Value("${datasource.weasy.password}")
	private String passwordWeasy;
	

	
	@Bean(name = "weasy")
	public DataSource dataSourceWeasy() {
		BasicDataSource dataSource = new BasicDataSource();

		dataSource.setDriverClassName(this.driverclassWeasy);
		dataSource.setUrl(this.urlWeasy);
		dataSource.setUsername(this.usernameWeasy);
		dataSource.setPassword(this.passwordWeasy);


		return dataSource;
	}

	

	@Bean
	public PlatformTransactionManager hostingsTransactionManager() {
		DataSourceTransactionManager manager = new DataSourceTransactionManager();
		manager.setDataSource(dataSourceWeasy());
		return manager;
	}

	 

}
