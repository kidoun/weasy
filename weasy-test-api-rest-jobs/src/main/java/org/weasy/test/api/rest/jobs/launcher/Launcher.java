package org.weasy.test.api.rest.jobs.launcher;


import org.springframework.boot.SpringApplication;
import org.weasy.test.api.rest.jobs.configuration.contextJobs.AppConfig;

public class Launcher {
	

	public static void main(String[] args) {
      
		SpringApplication.run(AppConfig.class, args);
		  

	}

}
