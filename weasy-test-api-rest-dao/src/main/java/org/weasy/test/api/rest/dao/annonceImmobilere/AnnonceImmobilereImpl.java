package org.weasy.test.api.rest.dao.annonceImmobilere;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.weasy.test.api.rest.dao.entity.AnnonceImmobiliere;

@Repository
public class AnnonceImmobilereImpl implements IannonceImmobiliereDao {
	
private NamedParameterJdbcTemplate jdbcTemplate;
private static String REQ_SELECT = "select * from annonceimmobiliere";
	
	@Autowired
	@Qualifier("weasy")
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}
	
	
	private static final class AnnonceImmobiliereMapper implements RowMapper<AnnonceImmobiliere> {

		public AnnonceImmobiliere mapRow(ResultSet rs, int rowNum) throws SQLException {
			AnnonceImmobiliere annonceImmobiliere = new AnnonceImmobiliere();
		    
			annonceImmobiliere.setIdAnnonceImmobiliere(rs.getInt("idAnnonceImmobiliere"));
			annonceImmobiliere.setTitre(rs.getString("titre"));
			annonceImmobiliere.setDescriptif(rs.getString("descriptif"));
			annonceImmobiliere.setAdresse(rs.getString("adresse"));
			annonceImmobiliere.setSurface((Long)rs.getObject("surface"));
			annonceImmobiliere.setPrix((Long)rs.getObject("prix"));


			return annonceImmobiliere;
		}
	}


	@Override
	public List<AnnonceImmobiliere> GetListOfAllAnnonceImmobiliere() throws Exception {	
		 return this.jdbcTemplate.query(REQ_SELECT, new AnnonceImmobiliereMapper());
	}


	@Override
	public void InsertAnnonceImmobiliere(AnnonceImmobiliere annonceImmobiliere) throws Exception {
		String query = "insert into annonceImmobiliere (titre, descriptif,adresse ,surface, prix) values (:titre,:descriptif,:adresse,:surface,:prix)";

		Map<String, Object> map = new HashMap<String, Object>();

		map.put("titre", annonceImmobiliere.getTitre());
		map.put("descriptif", annonceImmobiliere.getDescriptif());
		map.put("adresse", annonceImmobiliere.getAdresse());
		map.put("surface", annonceImmobiliere.getSurface());
		map.put("prix", annonceImmobiliere.getPrix());
 	
		 jdbcTemplate.update(query, map);
	}


	@Override
	public AnnonceImmobiliere FindAnnonceImmobiliereById(Long id) throws Exception {
		Map<String, Long> map = new HashMap<String, Long>();
		map.put("id", id);

		AnnonceImmobiliere anonceImmobiliere = this.jdbcTemplate.queryForObject(REQ_SELECT + " where idAnnonceImmobiliere= :id", map,
				new AnnonceImmobiliereMapper());
		return anonceImmobiliere;
	}

}
