package org.weasy.test.api.rest.dao.annonceImmobilere;

import java.util.List;

import org.weasy.test.api.rest.dao.entity.AnnonceImmobiliere;

public interface IannonceImmobiliereDao {
	List<AnnonceImmobiliere> GetListOfAllAnnonceImmobiliere() throws Exception;
	void       InsertAnnonceImmobiliere(AnnonceImmobiliere annonceImmobiliere) throws Exception;
	AnnonceImmobiliere FindAnnonceImmobiliereById(Long id) throws Exception;
	

}
